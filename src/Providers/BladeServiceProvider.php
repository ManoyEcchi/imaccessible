<?php

namespace Imaccessible\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('hasAccess', function ($accessName) {
            // sample userAccessNames
            $userAccessNames = Auth::user()->access_names;
            
            return in_array($accessName, $userAccessNames);
        });
    }
}
