<?php

namespace Imaccessible\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Auth\SessionGuard;
use Illuminate\Support\Facades\Auth;

class ImaccessibleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
          // Export the migration
          if (! class_exists('CreateUserAccessiblesTable')) {
            $this->publishes([
              // __DIR__ . '/../../database/migrations/create_accessible_groups_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_accessible_groups_table.php'),
              // __DIR__ . '/../../database/migrations/create_accessible_items_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_accessible_items_table.php'),
              __DIR__ . '/../../database/migrations/create_user_accessibles_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_user_accessibles_table.php'),
            ], 'migrations');
          }
        }

        SessionGuard::macro('hasAccess', function ($accessName) {
          $userAccessNames = Auth::user()->access_names;
          return in_array($accessName, $userAccessNames);
        });

        SessionGuard::macro('guardAccess', function ($accessName, $callback = null) {
          $userAccessNames = Auth::user()->access_names;
          $access = in_array($accessName, $userAccessNames);

          if (!$access) {
            if ($callback) {
              return $callback;
            } else {
              abort(404);
            }
          }
        });
    }
}
