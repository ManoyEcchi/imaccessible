<?php

namespace Imaccessible\Traits;

use Carbon\Carbon;
use Imaccessible\Models\UserAccessible;

trait Imaccessible
{
    public function accessibles() {
        return $this->hasMany(UserAccessible::class);
    }

    public function getAccessNamesAttribute() {
        return $this->accessibles()->where('revoke', 0)->pluck('name')->toArray();
    }

    public function createAccess($access_name) {
        $this->accessibles()->updateOrCreate([
            'name' => $access_name,
        ], [
            'revoke' => 0
        ]);
    }

    public function revokeAccess($access_name) {
        $this->accessibles()->where('name', $access_name)->update([
            'revoke' => 1
        ]);
    }
}