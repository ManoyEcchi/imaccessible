<?php

namespace Imaccessible;

class Imaccessible
{
    public static function verifyAccess($userAccessNames) {
        $accessNames = self::getMethodAccessName();

        if (count(array_intersect($userAccessNames, $accessNames)) or count($accessNames) == 0) {
            return true;
        }

        return false;
    }

    public static function getControllerRule() {
        list($controller, $method) = explode('@', \Route::currentRouteAction());

        return \App::call("$controller@accessRules");
    }

    public static function getMethodAccessName() {
        list($controller, $methodName) = explode('@', \Route::currentRouteAction());

        $rules = self::getControllerRule();

        $rules = array_filter($rules, function($item) use ($methodName) {
            return in_array($methodName, $item);
        });

        return array_keys($rules);
    }
}