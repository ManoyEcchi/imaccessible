<?php

namespace Imaccessible\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessibleItem extends Model
{
    use HasFactory, SoftDeletes;
}
